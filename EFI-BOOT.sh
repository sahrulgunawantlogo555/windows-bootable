# Extract boot load segment address and size

#BOOT_LOAD_SEG="$(dumpet -i "${WIN10_IMG}" | grep "Media load segment: " | cut -d ':' -f2 | cut -d ' ' -f2)"
#BOOT_LOAD_SIZE="$(dumpet -i "${WIN10_IMG}" | grep "Load Sectors: " | grep -o "[^:]*$" | cut -d ' ' -f2 | head -1)"

mkisofs \
  -no-emul-boot \
  -b BOOT/ETFSBOOT.COM \
  -boot-load-seg 0x0 \
  -boot-load-size 8 \
  -eltorito-alt-boot \
  -e EFI/BOOT/BOOTX64.EFI \
  -no-emul-boot \
  -iso-level 2 \
  -boot-info-table \
  -udf \
  -D \
  -N \
  -relaxed-filenames \
  -allow-lowercase \
  -o win10.iso \
  Windows
