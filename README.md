## Contents in Boot Files
BOOT-SECTOR/
├── BOOT
│   ├── BCD
│   ├── BCD.LOG
│   ├── BCD.LOG1
│   ├── BCD.LOG2
│   ├── BOOT.SDI
│   ├── BOOTFIX.BIN
│   ├── EFISYS.BIN
│   ├── EFISYS_NOPROMPT.BIN
│   ├── ETFSBOOT.COM
│   └── FONTS
│       ├── CHS_BOOT.TTF
│       ├── CHT_BOOT.TTF
│       ├── JPN_BOOT.TTF
│       ├── KOR_BOOT.TTF
│       └── WGL4_BOOT.TTF
├── BOOTMGR
├── BOOTMGR.EFI
├── EFI
│   ├── BOOT
│   │   └── BOOTX64.EFI
│   └── MICROSOFT
│       └── BOOT
│           ├── BCD
│           └── FONTS
│               ├── CHS_BOOT.TTF
│               ├── CHT_BOOT.TTF
│               ├── JPN_BOOT.TTF
│               ├── KOR_BOOT.TTF
│               └── WGL4_BOOT.TTF
└── [BOOT]
    └── Boot-NoEmul.img

9 directories, 24 files

```
apt install qemu-utils
```
```
apt install mkisofs -y
```
```
apt install nano -y
```
## Package Already Installed
```
nano boot.sh
```
```
mkisofs \
  -no-emul-boot \
  -b BOOT/ETFSBOOT.COM \
  -boot-load-seg 0x0 \
  -boot-load-size 8 \
  -eltorito-alt-boot \
  -e EFI/BOOT/BOOTX64.EFI \
  -no-emul-boot \
  -iso-level 2 \
  -boot-info-table \
  -udf \
  -D \
  -N \
  -relaxed-filenames \
  -allow-lowercase \
  -o win10.iso \
  BOOT-SECTOR
```
# How to Save Files
CTRL + X then Y then enter

# Extract Files 
BOOT-SECTOR.7z

# How to Make Windows Boot PE
Required Files Example
----------------------------------------
Program Files
ProgramData
Users
Windows
----------------------------------------
Add Into Folder BOOT-SECTOR

# If Already Added
Execute Command Now
```
sh boot.sh
```

# You just have to wait for it to finish
## Password Data Archive 
- [ ] [Password : sahrulgunawancyber]